#include <queue>
#include <mutex>
#include <condition_variable>
#include <utility>
#include <thread>
#include <memory>

class Task
{
public:
	virtual double wait() = 0;
	virtual void run() = 0;
	virtual void push_argument(double arg) = 0;
};

class EmptyTask:public Task
{
	double wait() {return 0;}
	void run() {}
	void push_argument(double arg) {};
};

class task_queue
{
	std::queue<std::shared_ptr<Task>> m_q;
	std::condition_variable m_cv;
	std::mutex* m_pMtx;
	bool m_fStop = false;
public:
	task_queue(std::mutex& mtxScheduler):m_pMtx(&mtxScheduler) {}
	template <class TaskRef> void push(TaskRef&& tsk)
	{
		std::unique_lock<std::mutex> lock(*m_pMtx);
		m_q.push(std::forward<TaskRef>(tsk));
		m_cv.notify_one();
	}
	std::shared_ptr<Task> pop(std::unique_lock<std::mutex>* pLock = nullptr)
	{
		std::unique_lock<std::mutex> lock(*m_pMtx);
		while (m_q.empty() && !m_fStop)
			m_cv.wait(lock);
		if (m_fStop)
			return std::shared_ptr<Task>(nullptr);
		auto tsk = std::move(m_q.front());
		m_q.pop();
		if (pLock)
			*pLock = std::move(lock);
		return tsk;
	}
	void unblock_all()
	{
		std::lock_guard<std::mutex> lock(*m_pMtx);
		m_fStop = true;
		m_cv.notify_all();
	}
	bool empty() const
	{
		std::unique_lock<std::mutex> lock(*m_pMtx);
		return m_q.empty();
	}
	bool empty(std::unique_lock<std::mutex>&) const
	{
		return m_q.empty();
	}
};

class thread_pool
{
	unsigned m_cFreeThreads, m_cMaxThreads;
	std::mutex* m_pMtx;
	task_queue * m_pq;
	std::vector<std::thread> m_threads;
	std::condition_variable m_cvIsFull; //m_cFreeThreads ==m_cMaxThreads
	void Worker()
	{
		std::shared_ptr<Task> pTask;
		std::unique_lock<std::mutex> lock;
		while (bool(pTask = m_pq->pop(&lock)))
		{
			--m_cFreeThreads;
			lock.unlock();
			pTask->run();
			{
				lock.lock();
				if (++m_cFreeThreads == m_cMaxThreads && m_pq->empty(lock))
					m_cvIsFull.notify_all();
				lock.unlock();
			}
		}
	}
public:
	thread_pool() = default;
	thread_pool(task_queue* queue, std::mutex& mtxScheduler,
		unsigned cMaxThreads = std::thread::hardware_concurrency()):m_cFreeThreads(cMaxThreads), m_cMaxThreads(cMaxThreads), m_pMtx(&mtxScheduler), m_pq(queue)
	{
		m_threads.reserve(cMaxThreads);
		while (cMaxThreads-- > 0)
			m_threads.emplace_back(&thread_pool::Worker, std::ref(*this));
	}
	~thread_pool() 
	{
		for (std::thread& thr:m_threads)
			thr.join();
	}
	void wait_for_fullness()
	{
		std::unique_lock<std::mutex> lock(*m_pMtx);
		while (!m_pq->empty(lock) || m_cFreeThreads < m_cMaxThreads)
			m_cvIsFull.wait(lock);
		return;
	}
};

class schedule
{
	std::mutex mtx;
	task_queue m_q;
	thread_pool m_pool;
public:
	template <class TaskRef> void add_task(TaskRef&& tsk)
	{
		m_q.push(std::forward<TaskRef>(tsk));
	}
	schedule():m_q(mtx), m_pool(&m_q, mtx) {}
	~schedule()
	{
		m_q.unblock_all();
	}
	void wait()
	{
		m_pool.wait_for_fullness();
	}
};

class Fibonacci:public Task
{
	unsigned n = 0;
	schedule *ps;
	unsigned my_result;
	unsigned *shared_result;
public:
	Fibonacci(schedule* sched, unsigned *shared_res)
	:ps(sched), shared_result(shared_res) {}
	double wait()
	{
		ps->wait();
		return *shared_result;
	}
	void run() 
	{
		if (n <=1)
		{
			++*shared_result;
		}else
		{
			auto t1 = std::make_shared<Fibonacci>(ps, shared_result);
			t1->push_argument(n - 1);
			ps->add_task(std::move(t1));
			auto t2 = std::make_shared<Fibonacci>(ps, shared_result);
			t2->push_argument(n - 2);
			ps->add_task(std::move(t2));
		}
	}
	void push_argument(double arg)
	{
		n = unsigned(arg);
	};
};

#include <iostream>

int main(int argc, char** argv)
{
	unsigned result = 0;
	schedule sched;
	Fibonacci task{&sched, &result};
	task.push_argument(10);
	task.run();
	std::cout << task.wait() << "\n";
	return 0;
}