#include <queue>
#include <vector>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <type_traits>

#if __cplusplus > 201700 || defined(_MSC_VER) && _MSVC_LANG > 201700
#include <optional>
using std::optional;
using std::nullopt_t;
using std::nullopt;
#else

struct nullopt_t {};
constexpr nullopt_t nullopt;

template <class T>
struct optional
{
	optional() = default;
	constexpr optional(nullopt_t) {}
	template <class U, class = typename std::enable_if<std::is_constructible<T, U&&>::value>::type>
	optional(U&& val)
	{
		m_ptr = new (&m_strg) T(std::forward<U>(val));
	}
	optional(const optional& right) {*this = right;}
	optional(optional&& right) {*this = right;}
	optional& operator=(const optional& right)
	{
		this->assign(right, [](T* ptr, const T& right) -> void {new (ptr) T(right);});
		return *this;
	}
	optional& operator=(optional&& right)
	{
		this->assign(right, [](T* ptr, T& right) -> void {new (ptr) T(std::move(right));});
		return *this;
	}
	constexpr bool has_value() const {return m_ptr != nullptr;}
	constexpr explicit operator bool() const {return has_value();}
	constexpr const T& value() const {return *m_ptr;}
	constexpr T& value() {return *m_ptr;}
	constexpr const T* operator->() const {return &this->value();}
	constexpr T* operator->() {return &this->value();}
	constexpr const T& operator*() const {return this->value();}
	constexpr T& operator*() {return this->value();}
private:
	typename std::aligned_storage<sizeof(T), alignof(T)>::type m_strg;
	T* m_ptr = nullptr;
private:
	template <class OptionalRight, class Ctor>
	void assign(OptionalRight&& right, Ctor&& ctor)
	{
		if (this != &right)
		{
			if (m_ptr)
			{
				m_ptr->~T();
				m_ptr = nullptr;
			}
			if (right.has_value())
			{
				m_ptr = reinterpret_cast<T*>(&m_strg);
				std::forward<Ctor>(ctor)(m_ptr, right.value());
			}
		}
		return *this;
	}

};
#endif //__cplusplus

template <class Task>
class task_queue
{
	std::queue<Task> m_q;
	mutable std::mutex m_mtx;
	mutable std::condition_variable m_cvNotEmpty;
	bool m_fStopPop = false;
public:
	template <class TaskParam>
	void push(TaskParam&& task)
	{
		std::lock_guard<std::mutex> lock(m_mtx);
		m_q.emplace(std::forward<TaskParam>(task));
		m_cvNotEmpty.notify_all();
	}
	optional<Task> pop()
	{
		std::unique_lock<std::mutex> lock(m_mtx);
		while (m_q.empty() && !m_fStopPop)
			m_cvNotEmpty.wait(lock);
		if (!m_fStopPop)
		{
			Task task = m_q.front();
			m_q.pop();
			return task;
		}else
			return nullopt; //INVALID task.
	}
	void stop_active_threads()
	{
		std::lock_guard<std::mutex> lock(m_mtx);
		m_fStopPop = true;
		m_cvNotEmpty.notify_all();
	}
	bool empty() const
	{
		std::lock_guard<std::mutex> lock(m_mtx);
		return m_q.empty();
	}
};

template <class Task>
class thread_pool
{
	std::vector<std::thread> m_threads;
	unsigned m_cMaxFreeThreads = std::thread::hardware_concurrency(), m_cFreeThreads = 0;
	std::mutex m_mtx;
	std::condition_variable m_cvAllDone;
	bool m_fStop = false;
	task_queue<Task> m_q;
	void Worker()
	{
		while (true)
		{
			std::unique_lock<std::mutex> lock(m_mtx);
			if (++m_cFreeThreads == m_cMaxFreeThreads && m_q.empty())
				m_cvAllDone.notify_all();
			if (m_fStop)
				break;
			auto task = m_q.pop(/*[this]() {--m_cFreeThreads;}*/);
			if (!bool(task)) 
				break;
			--m_cFreeThreads;
			lock.unlock();
			task.value()();
		}
	}
public:
	explicit thread_pool(unsigned cMaxFreeThreads = std::thread::hardware_concurrency()):m_cMaxFreeThreads(cMaxFreeThreads)
	{
		m_threads.reserve(m_cMaxFreeThreads);
		for (unsigned iThread = 0; iThread < m_cMaxFreeThreads; ++iThread)
			m_threads.emplace_back(&thread_pool::Worker, std::ref(*this));
	}
	~thread_pool()
	{
		m_q.stop_active_threads();
		{
			std::lock_guard<std::mutex> lock(m_mtx);
			m_fStop = true;
		}
		for (auto& thr:m_threads)
			thr.join();
	}
	template <class TaskParam>
	void add_task(TaskParam&& task)
	{
		m_q.push(std::forward<TaskParam>(task));
	}
};

template <class Task>
class scheduler
{
	thread_pool<Task> m_pool;
public:
	scheduler() = default;
	template <class TaskParam>
	void add_task(TaskParam&& task)
	{
		m_pool.add_task(std::forward<TaskParam>(task));
	}
};

class fibonacci_task
{
	unsigned m_n;
	scheduler<fibonacci_task>* m_pSched;
	struct result_storage
	{
		unsigned m_val = 0;
		unsigned task_ctr = 1;
		std::mutex m_mtx;
		std::condition_variable m_cvAllTasksDone;
	};
	std::shared_ptr<result_storage> m_pResult;

	fibonacci_task(scheduler<fibonacci_task>& sched, unsigned n, const std::shared_ptr<result_storage>& pResult):m_pSched(&sched), m_n(n), m_pResult(pResult) {}
public:
	fibonacci_task(scheduler<fibonacci_task>& sched, unsigned n):fibonacci_task(sched, n, std::make_shared<result_storage>()) {}
	fibonacci_task& operator=(const fibonacci_task&) = default;
	fibonacci_task& operator=(fibonacci_task&&) = default;
	fibonacci_task(const fibonacci_task& right)
	{
		*this = right;
	}
	fibonacci_task(fibonacci_task&& right)
	{
		*this = std::move(right);
	}
	void operator()()
	{
		if (m_n < 2)
		{
			std::lock_guard<std::mutex> lock(m_pResult->m_mtx);
			m_pResult->m_val += m_n;
			if (!--m_pResult->task_ctr)
				m_pResult->m_cvAllTasksDone.notify_all();
		}else
		{
			m_pSched->add_task(fibonacci_task(*m_pSched, m_n - 1, m_pResult));
			m_pSched->add_task(fibonacci_task(*m_pSched, m_n - 2, m_pResult));
			++m_pResult->task_ctr;
		}
	}
	unsigned get_result() const
	{
		//m_pSched->wait();
		std::unique_lock<std::mutex> lock(m_pResult->m_mtx);
		while (m_pResult->task_ctr > 0)
			m_pResult->m_cvAllTasksDone.wait(lock);
		return m_pResult->m_val;
	}
};

#include <iostream>

int main(int argc, char** argv)
{
	scheduler<fibonacci_task> sched;
	auto task = fibonacci_task(sched, 10);
	sched.add_task(task);
	std::cout << task.get_result() << "\n";
	return 0;
}